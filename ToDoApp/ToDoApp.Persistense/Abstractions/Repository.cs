﻿using System.Data.SqlClient;
using ToDoApp.Persistense.Configurations;

namespace ToDoApp.Persistense.Abstractions;

public abstract class Repository
{
    public SqlConnection CreateOpenedConnection()
    {
        var connectionString = DatabaseConfiguration.Instance.ConnectionString;
        var connection = new SqlConnection(connectionString);
        connection.Open();
        return connection;
    }
}
