﻿using Dapper;
using ToDoApp.Domain.Lists;
using ToDoApp.Persistense.Abstractions;

namespace ToDoApp.Persistense.Repositories;

public sealed class ListRepository : Repository, IListRepository
{
    public void Add(ToDoList model)
    {
        using var connection = CreateOpenedConnection();
        connection.Query("insert into Lists (Title, Description) values (@Title, @Description)", model);
    }

    public void Delete(int id)
    {
        using var connection = CreateOpenedConnection();
        connection.Query("delete from Lists where Id = @Id", new { Id = id });
    }

    public ToDoList? Get(int id)
    {
        using var connection = CreateOpenedConnection();
        return connection.QueryFirstOrDefault<ToDoList>("select * from Lists where Id = @Id", new { Id = id });
    }

    public IReadOnlyCollection<ToDoList> GetAll()
    {
        using var connection = CreateOpenedConnection();
        return connection.Query<ToDoList>("select * from Lists").ToList();
    }

    public void Update(ToDoList model)
    {
        using var connection = CreateOpenedConnection();
        connection.Query("update Lists set Title = @Title, Description = @Description where Id = @Id", model);
    }
}
