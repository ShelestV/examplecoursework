﻿using Dapper;
using ToDoApp.Domain.Users;
using ToDoApp.Persistense.Abstractions;

namespace ToDoApp.Persistense.Repositories;

public sealed class UserRepository : Repository, IUserRepository
{
    public void Add(User user)
    {
        using var connection = CreateOpenedConnection();
        connection.Query("INSERT INTO Users (Name, Password, Role) VALUES (@Name, @Password, 'SIMPLE')", user);
    }

    public User? GetByName(string name)
    {
        using var connection = CreateOpenedConnection();
        return connection.QuerySingleOrDefault<User>("SELECT * FROM Users WHERE Name = @Name", new { Name = name });
    }
}
