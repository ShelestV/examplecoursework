﻿namespace ToDoApp.WindowsView.Controls;

public static class ControlsCreator
{
    public static Button CreateButton(string text, int x, int y, bool visible = true)
    {
        return new Button
        {
            Size = new Size(ControlsConstants.ButtonWidth, ControlsConstants.ButtonHeight),
            Location = new Point(x, y),
            Text = text,
            Visible = visible,
        };
    }

    public static TextBox CreateTextBox(string placeholder, int x, int y, char passwordChar = default, bool visible = true)
    {
        return new TextBox
        {
            Size = new Size(ControlsConstants.TextBoxWidth, ControlsConstants.TextBoxHeight),
            Location = new Point(x, y),
            PlaceholderText = placeholder,
            PasswordChar = passwordChar,
            Visible = visible,
        };
    }

    public static DataGridView CreateDataGridView(int x, int y)
    {
        return new DataGridView
        {
            AllowUserToAddRows = false,
            AllowUserToDeleteRows = false,
            AllowUserToResizeRows = false,
            ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
            Location = new Point(x, y),
            Name = "listsDataGridView",
            RowTemplate = { Height = ControlsConstants.TextBoxHeight },
        };
    }
}
