﻿using ToDoApp.WindowsView.Controls.Helpers;
using CC = ToDoApp.WindowsView.Controls.ControlsConstants;

namespace ToDoApp.WindowsView.Controls;

public sealed class NavigationPanel : Panel
{
    private readonly Button _logInButton;
    private readonly Button _registerButton;
    private readonly Button _logOutButton;
    private readonly Button _moveToLists;
    private readonly Button _moveToTasks;

    public event EventHandler? MoveToAuthorization
    {
        add => _logInButton.Click += value;
        remove => _logInButton.Click -= value;
    }

    public event EventHandler? MoveToRegistrationPanel
    {
        add => _registerButton.Click += value;
        remove => _registerButton.Click -= value;
    }

    public event EventHandler? LogOutClick
    {
        add => _logOutButton.Click += value;
        remove => _logOutButton.Click -= value;
    }

    public event EventHandler? MoveToListsClick
    {
        add => _moveToLists.Click += value;
        remove => _moveToLists.Click -= value;
    }

    public event EventHandler? MoveToTasksClick
    {
        add => _moveToTasks.Click += value;
        remove => _moveToTasks.Click -= value;
    }

    public new Size Size
    {
        get => base.Size;
        set
        {
            base.Size = value;
            _logOutButton.Location = new Point(CC.Gap, value.Height - CC.Gap - CC.ButtonHeight);
        }
    }

    public NavigationPanel()
    {
        _logInButton = ControlsCreator.CreateButton("Log in", CC.Gap, PositionCalculator.CalculateY(CC.Gap));
        _registerButton = ControlsCreator.CreateButton("Sign In", CC.Gap, PositionCalculator.CalculateY(CC.Gap, _logInButton));
        _logOutButton = ControlsCreator.CreateButton("Log out", CC.Gap, Height - CC.Gap - CC.ButtonHeight, false);
        _moveToLists = ControlsCreator.CreateButton("Lists", CC.Gap, PositionCalculator.CalculateY(CC.Gap), false);
        _moveToTasks = ControlsCreator.CreateButton("Tasks", CC.Gap, PositionCalculator.CalculateY(CC.Gap, _moveToLists), false);

        this.InitializeComponent();
    }

    private void InitializeComponent()
    {
        SuspendLayout();

        _logOutButton.Click += OnLogOutClicked;

        Controls.Add(_logInButton);
        Controls.Add(_registerButton);
        Controls.Add(_logOutButton);
        Controls.Add(_moveToLists);
        Controls.Add(_moveToTasks);

        ResumeLayout(false);
    }

    public void OnLogOutClicked(object? sender, EventArgs e)
    {
        _logInButton.Visible = true;
        _registerButton.Visible = true;
        _logOutButton.Visible = false;
        _moveToLists.Visible = false;
        _moveToTasks.Visible = false;

        Cache.User = null;
    }

    public void OnLoggedIn(object? sender, EventArgs e)
    {
        _logInButton.Visible = false;
        _registerButton.Visible = false;
        _logOutButton.Visible = true;
        _moveToLists.Visible = true;
        _moveToTasks.Visible = true;
    }
}
