﻿using ToDoApp.WindowsView.Controls.Events;
using ToDoApp.WindowsView.Controls.Helpers;
using ToDoApp.WindowsView.Logging;
using ToDoApp.WindowsView.ViewModels.Users;
using CC = ToDoApp.WindowsView.Controls.ControlsConstants;

namespace ToDoApp.WindowsView.Controls;

public sealed class AuthorizationPanel : Panel
{
    private static readonly object logInEventKey = new();

    private readonly Logger _logger = Logger.Create();

    private readonly TextBox _loginTextBox;
    private readonly TextBox _passwordTextBox;
    private readonly Button _logInButton;

    public event LoginEventHandler? LogIn
    {
        add => Events.AddHandler(logInEventKey, value);
        remove => Events.RemoveHandler(logInEventKey, value);
    }

    private readonly AuthViewModel _authViewModel;

    public AuthorizationPanel()
    {
        _loginTextBox = ControlsCreator.CreateTextBox("User name...", CC.Gap, PositionCalculator.CalculateY(CC.Gap));
        _passwordTextBox = ControlsCreator.CreateTextBox("Password...", CC.Gap, PositionCalculator.CalculateY(CC.Gap, _loginTextBox), '*');
        _logInButton = ControlsCreator.CreateButton("Log in", CC.Gap + _passwordTextBox.Width - CC.ButtonWidth, PositionCalculator.CalculateY(CC.Gap, _loginTextBox, _passwordTextBox));

        _authViewModel = new AuthViewModel();

        this.InitializeComponent();
    }

    private void InitializeComponent()
    {
        SuspendLayout();

        _authViewModel.Initialize(_loginTextBox, _passwordTextBox);

        _logInButton.Click += OnLogInClicked;

        Controls.Add(_loginTextBox);
        Controls.Add(_passwordTextBox);
        Controls.Add(_logInButton);

        ResumeLayout(false);
    }

    private void OnLogInClicked(object? sender, EventArgs e)
    {
        if (!_authViewModel.TryLogin())
        {
            _logger.Error(Error.AuthError("Invalid login or password"));
            return;
        }

        var args = new LoginEventArgs
        {
            UserName = _loginTextBox.Text,
            Password = _passwordTextBox.Text,
        };

        var handler = Events[logInEventKey] as LoginEventHandler;
        handler?.Invoke(sender, args);
    }
}
