﻿using ToDoApp.Domain.Abstractions;
using ToDoApp.WindowsView.ViewModels.Lists;

using CC = ToDoApp.WindowsView.Controls.ControlsConstants;

namespace ToDoApp.WindowsView.Controls;

public abstract class ModelPanel<TModel> : Panel where TModel : Model
{
    private readonly DataGridView _listsDataGridView;
    private readonly Button _saveButton;

    private readonly ListsViewModel _viewModel;

    public new Size Size
    {
        get => base.Size;
        set
        {
            base.Size = value;

            _listsDataGridView.Size = new Size(value.Width - CC.Gap * 2, value.Height - CC.Gap * 3 - _saveButton.Height);
            _saveButton.Location = new Point(value.Width - CC.Gap - CC.ButtonWidth, value.Height - CC.Gap - CC.ButtonHeight);
        }
    }

    public new bool Visible
    {
        get => base.Visible;
        set
        {
            base.Visible = value;

            _viewModel.UpdateGrid();
        }
    }

    protected ModelPanel()
    {
        _viewModel = new ListsViewModel();

        _saveButton = ControlsCreator.CreateButton("Save", Width - CC.Gap - CC.ButtonWidth, Height - CC.Gap - CC.ButtonHeight);
        _listsDataGridView = ControlsCreator.CreateDataGridView(CC.Gap, CC.Gap);

        _viewModel.Initialize(_listsDataGridView, _saveButton);

        this.InitializeComponent();
    }

    private void InitializeComponent()
    {
        SuspendLayout();

        Controls.Add(_listsDataGridView);
        Controls.Add(_saveButton);

        ResumeLayout(false);
    }
}
