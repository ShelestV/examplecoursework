﻿namespace ToDoApp.WindowsView.Controls;

public static class ControlsConstants
{
    public const int Gap = 10;

    public const int ButtonWidth = 100;
    public const int ButtonHeight = 30;

    public const int TextBoxWidth = 200;
    public const int TextBoxHeight = 30;
}
