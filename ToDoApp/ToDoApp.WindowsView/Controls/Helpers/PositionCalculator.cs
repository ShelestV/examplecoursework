﻿namespace ToDoApp.WindowsView.Controls.Helpers;

public static class PositionCalculator
{
    public static int CalculateY(int gap, params Control[] controlsBefore)
    {
        return gap + controlsBefore.Select(x => x.Height + gap).Sum();
    }
}
