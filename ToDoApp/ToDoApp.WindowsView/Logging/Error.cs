﻿namespace ToDoApp.WindowsView.Logging;

public class Error
{
    public string Title { get; }
    public string Message { get; }

    protected Error(string title, string message)
    {
        Title = title;
        Message = message;
    }

    public static Error AuthError(string message) => new("Authentication Error", message);
}
