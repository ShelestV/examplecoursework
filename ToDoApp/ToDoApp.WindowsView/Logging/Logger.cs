﻿namespace ToDoApp.WindowsView.Logging;

public sealed class Logger
{
    public void Error(Error error)
    {
        MessageBox.Show(error.Message, error.Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    public static Logger Create() => new();
}
