﻿using ToDoApp.Domain.Users;

namespace ToDoApp.WindowsView;

internal static class Cache
{
    public static User? User { get; set; }
}
