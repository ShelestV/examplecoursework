﻿using ToDoApp.Domain.Abstractions;

namespace ToDoApp.WindowsView.ViewModels;

public abstract class ViewModel<TModel> where TModel : Model
{
    public const string IdColumnName = "Id";

    public int Id { get; set; }

    public abstract TModel ToModel();
}
