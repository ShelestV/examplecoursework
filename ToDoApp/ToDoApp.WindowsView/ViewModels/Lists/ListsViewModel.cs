﻿using ToDoApp.Domain.Lists;
using ToDoApp.Persistense.Repositories;

namespace ToDoApp.WindowsView.ViewModels.Lists;

public sealed class ListsViewModel : GridViewModel<ToDoList, ListViewModel>
{
    public ListsViewModel() : base(new ListRepository())
    {
    }

    protected override ListViewModel Parse(ToDoList model) => 
        ListViewModel.Parse(model);

    protected override DataGridViewColumn[] GetColumns(bool isAdmin) => 
        ListViewModel.GetColumns(isAdmin);

    protected override IEnumerable<object?> ExtractColumnValues(ListViewModel viewModel)
    {
        yield return viewModel.Id;
        yield return viewModel.Title;
        yield return viewModel.Description;
    }

    protected override bool TryParse(DataGridViewRow row, out ListViewModel viewModel) => 
        ListViewModel.TryParse(row, out viewModel);

    protected override bool Equals(ListViewModel originalViewModel, ListViewModel viewModel) => 
        originalViewModel.Equals(viewModel.Title, viewModel.Description);
}
