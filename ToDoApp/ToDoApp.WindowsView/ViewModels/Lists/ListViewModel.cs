﻿using ToDoApp.Domain.Lists;

namespace ToDoApp.WindowsView.ViewModels.Lists;

public sealed class ListViewModel : ViewModel<ToDoList>
{
    public const string TitleColumnName = "Title";
    public const string DescriptionColumnName = "Description";

    public string Title { get; set; } = string.Empty;
    public string? Description { get; set; }

    public override ToDoList ToModel()
    {
        return new ToDoList
        {
            Id = Id,
            Title = Title,
            Description = Description
        };
    }

    public bool Equals(string title, string? description)
    {
        return Title.Equals(title) && EqualsByDescription(description);
    }

    private bool EqualsByDescription(string? description)
    {
        return (Description == null && description == null) ||
            (Description != null && description != null && Description.Equals(description));
    }

    public static DataGridViewColumn[] GetColumns(bool isAdmin) => new DataGridViewColumn[]
    {
        new DataGridViewTextBoxColumn
        {
            Name = IdColumnName,
            DataPropertyName = IdColumnName,
            HeaderText = IdColumnName,
            Visible = isAdmin
        },
        new DataGridViewTextBoxColumn
        {
            Name = TitleColumnName,
            DataPropertyName = TitleColumnName,
            HeaderText = TitleColumnName
        },
        new DataGridViewTextBoxColumn
        {
            Name = DescriptionColumnName,
            DataPropertyName = DescriptionColumnName,
            HeaderText = DescriptionColumnName
        }
    };

    public static bool TryParse(DataGridViewRow row, out ListViewModel listViewModel)
    {
        listViewModel = new ListViewModel();

        var idString = row.Cells[IdColumnName].Value?.ToString();
        var id = int.TryParse(idString, out var parsedId) ? parsedId : -1;

        var title = row.Cells[TitleColumnName].Value?.ToString();
        if (string.IsNullOrWhiteSpace(title))
            return false;

        var description = row.Cells[DescriptionColumnName].Value?.ToString();

        listViewModel.Id = id;
        listViewModel.Title = title;
        listViewModel.Description = description;

        return true;
    }

    public static ListViewModel Parse(ToDoList list)
    {
        return new ListViewModel
        {
            Id = list.Id,
            Title = list.Title,
            Description = list.Description
        };
    }
}
