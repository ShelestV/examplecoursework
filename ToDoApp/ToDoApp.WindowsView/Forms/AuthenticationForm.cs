﻿using ToDoApp.WindowsView.ViewModels.Users;

namespace ToDoApp.WindowsView.Forms;

public partial class AuthenticationForm : Form
{
    private readonly Action<AuthenticationForm, bool> _callback;
    private readonly AuthViewModel _viewModel;

    private bool _isLoggedIn;

    public AuthenticationForm(Action<AuthenticationForm, bool> callback)
    {
        _callback = callback;
        _isLoggedIn = false;

        InitializeComponent();

        _viewModel = new AuthViewModel();
    }

    private void OnFormLoad(object sender, EventArgs e)
    {
        _viewModel.Initialize(nameTextBox, passwordTextBox);
    }

    private void OnCancelButtonClicked(object sender, EventArgs e)
    {
        Close();
    }

    private void OnLogInButtonClicked(object sender, EventArgs e)
    {
        _isLoggedIn = _viewModel.TryLogin();
        if (_isLoggedIn)
            Close();
        else
            MessageBox.Show("Invalid credentials", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    private void OnFormClosed(object sender, FormClosedEventArgs e)
    {
        _callback(this, _isLoggedIn);
    }
}
