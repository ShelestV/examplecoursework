﻿using ToDoApp.WindowsView.Controls;
using ToDoApp.WindowsView.Controls.Events;

namespace ToDoApp.WindowsView.Forms;

partial class MainForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        _navigationPanel = new NavigationPanel();
        _defaultPanel = new Panel();
        _authorizationPanel = new AuthorizationPanel();
        _listsPanel = new ListsPanel();
        this.SuspendLayout();
        // 
        // _navigationPanel
        // 
        _navigationPanel.Location = new Point(12, 12);
        _navigationPanel.Name = "_navigationPanel";
        _navigationPanel.Size = new Size(177, 426);
        _navigationPanel.TabIndex = 2;
        _navigationPanel.MoveToAuthorization += HidePanels;
        _navigationPanel.MoveToRegistrationPanel += HidePanels;
        _navigationPanel.LogOutClick += HidePanels;
        _navigationPanel.MoveToListsClick += HidePanels;
        _navigationPanel.MoveToTasksClick += HidePanels;
        _navigationPanel.MoveToAuthorization += MoveToAuthorizationPanel;
        _navigationPanel.MoveToRegistrationPanel += MoveToRegistrationPanel;
        _navigationPanel.LogOutClick += MoveToDefaultPanel;
        _navigationPanel.MoveToListsClick += MoveToListsPanel;
        _navigationPanel.MoveToTasksClick += MoveToTasksPanel;
        // 
        // _defaultPanel
        // 
        _defaultPanel.Location = new Point(195, 12);
        _defaultPanel.Name = "_defaultPanel";
        _defaultPanel.Size = new Size(593, 426);
        _defaultPanel.TabIndex = 3;
        //
        // _authorizationPanel
        //
        _authorizationPanel.Location = _defaultPanel.Location;
        _authorizationPanel.Name = "_authorizationPanel";
        _authorizationPanel.Size = _defaultPanel.Size;
        _authorizationPanel.TabIndex = _defaultPanel.TabIndex;
        _authorizationPanel.Visible = false;
        _authorizationPanel.LogIn += HidePanels;
        _authorizationPanel.LogIn += MoveToListsPanel;
        _authorizationPanel.LogIn += _navigationPanel.OnLoggedIn;
        //
        // _listsPanel
        //
        _listsPanel.Location = _defaultPanel.Location;
        _listsPanel.Name = "_listsPanel";
        _listsPanel.Size = _defaultPanel.Size;
        _listsPanel.TabIndex = _defaultPanel.TabIndex;
        _listsPanel.Visible = false;
        // 
        // MainForm
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(800, 450);
        Controls.Add(_defaultPanel);
        Controls.Add(_navigationPanel);
        Controls.Add(_authorizationPanel);
        Controls.Add(_listsPanel);
        Name = "MainForm";
        Text = "MainForm";
        this.ResumeLayout(false);
    }

    #endregion
    private NavigationPanel _navigationPanel;
    private Panel _defaultPanel;
    private AuthorizationPanel _authorizationPanel;
    private ListsPanel _listsPanel;
}