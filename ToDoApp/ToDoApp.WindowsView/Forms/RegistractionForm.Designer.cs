﻿namespace ToDoApp.WindowsView.Forms;

partial class RegistrationForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        nameTextBox = new TextBox();
        nameLabel = new Label();
        passwordLabel = new Label();
        passwordTextBox = new TextBox();
        registerButton = new Button();
        cancelButton = new Button();
        this.SuspendLayout();
        // 
        // nameTextBox
        // 
        nameTextBox.Location = new Point(158, 70);
        nameTextBox.Name = "nameTextBox";
        nameTextBox.Size = new Size(125, 27);
        nameTextBox.TabIndex = 0;
        // 
        // nameLabel
        // 
        nameLabel.AutoSize = true;
        nameLabel.Location = new Point(82, 73);
        nameLabel.Name = "nameLabel";
        nameLabel.Size = new Size(49, 20);
        nameLabel.TabIndex = 1;
        nameLabel.Text = "Name";
        // 
        // passwordLabel
        // 
        passwordLabel.AutoSize = true;
        passwordLabel.Location = new Point(82, 113);
        passwordLabel.Name = "passwordLabel";
        passwordLabel.Size = new Size(70, 20);
        passwordLabel.TabIndex = 3;
        passwordLabel.Text = "Password";
        // 
        // passwordTextBox
        // 
        passwordTextBox.Location = new Point(158, 110);
        passwordTextBox.Name = "passwordTextBox";
        passwordTextBox.PasswordChar = '*';
        passwordTextBox.Size = new Size(125, 27);
        passwordTextBox.TabIndex = 2;
        // 
        // registerButton
        // 
        registerButton.Location = new Point(175, 143);
        registerButton.Name = "registerButton";
        registerButton.Size = new Size(108, 27);
        registerButton.TabIndex = 4;
        registerButton.Text = "Sign In";
        registerButton.UseVisualStyleBackColor = true;
        registerButton.Click += this.OnSigninButtonClicked;
        // 
        // cancelButton
        // 
        cancelButton.Location = new Point(175, 176);
        cancelButton.Name = "cancelButton";
        cancelButton.Size = new Size(108, 29);
        cancelButton.TabIndex = 5;
        cancelButton.Text = "Cancel";
        cancelButton.UseVisualStyleBackColor = true;
        cancelButton.Click += this.OnCancelButtonClicked;
        // 
        // RegistrationForm
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(800, 450);
        Controls.Add(cancelButton);
        Controls.Add(registerButton);
        Controls.Add(passwordLabel);
        Controls.Add(passwordTextBox);
        Controls.Add(nameLabel);
        Controls.Add(nameTextBox);
        Name = "RegistrationForm";
        Text = "RegistrationForm";
        this.Load += this.OnFormLoad;
        this.FormClosed += this.OnFormClosed;
        this.ResumeLayout(false);
        this.PerformLayout();
    }

    #endregion

    private TextBox nameTextBox;
    private Label nameLabel;
    private Label passwordLabel;
    private TextBox passwordTextBox;
    private Button registerButton;
    private Button cancelButton;
}