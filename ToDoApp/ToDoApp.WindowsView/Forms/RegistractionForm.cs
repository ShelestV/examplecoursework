﻿using ToDoApp.WindowsView.ViewModels.Users;

namespace ToDoApp.WindowsView.Forms;

public partial class RegistrationForm : Form
{
    private readonly Action _callback;
    private readonly RegisterViewModel _viewModel;

    public RegistrationForm(Action callback)
    {
        _callback = callback;

        InitializeComponent();

        _viewModel = new RegisterViewModel();
    }

    private void OnSigninButtonClicked(object sender, EventArgs e)
    {
        if (!_viewModel.TryRegister())
            MessageBox.Show("User already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        
        Close();
    }

    private void OnFormLoad(object sender, EventArgs e)
    {
        _viewModel.Initialize(nameTextBox, passwordTextBox);
    }

    private void OnCancelButtonClicked(object sender, EventArgs e)
    {
        Close();
    }

    private void OnFormClosed(object sender, FormClosedEventArgs e)
    {
        _callback();
    }
}
