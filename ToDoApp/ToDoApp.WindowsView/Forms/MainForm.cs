﻿namespace ToDoApp.WindowsView.Forms;

public partial class MainForm : Form
{
    public MainForm()
    {
        InitializeComponent();
    }

    private void MoveToAuthorizationPanel(object sender, EventArgs args)
    {
        _authorizationPanel.Visible = true;
    }

    private void MoveToRegistrationPanel(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void MoveToDefaultPanel(object sender, EventArgs e)
    {
        _defaultPanel.Visible = true;
    }

    private void MoveToListsPanel(object sender, EventArgs e)
    {
        _listsPanel.Visible = true;
    }

    private void MoveToTasksPanel(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void HidePanels(object sender, EventArgs args)
    {
        _defaultPanel.Visible = false;
        _authorizationPanel.Visible = false;
        _listsPanel.Visible = false;
    }
}
