﻿using ToDoApp.Domain.Abstractions;

namespace ToDoApp.Domain.Lists;

public interface IListRepository : IRepository<ToDoList>
{
}
