﻿using ToDoApp.Domain.Abstractions;
using ToDoApp.Domain.Tasks;

namespace ToDoApp.Domain.Lists;

public sealed class ToDoList : Model
{
    public string Title { get; set; } = string.Empty;
    public string? Description { get; set; }
    public List<ToDoTask>? Tasks { get; set; }
}
