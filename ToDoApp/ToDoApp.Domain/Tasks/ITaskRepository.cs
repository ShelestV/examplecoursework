﻿using ToDoApp.Domain.Abstractions;

namespace ToDoApp.Domain.Tasks;

public interface ITaskRepository : IRepository<ToDoTask>
{
    IReadOnlyCollection<ToDoTask> GetByList(int listId);
}
