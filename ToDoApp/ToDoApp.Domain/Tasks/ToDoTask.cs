﻿using ToDoApp.Domain.Abstractions;
using ToDoApp.Domain.Lists;

namespace ToDoApp.Domain.Tasks;

public sealed class ToDoTask : Model
{
    public string Title { get; set; } = string.Empty;
    public string? Description { get; set; }
    public bool? IsCompleted { get; set; }
    public int? ListId { get; set; }
    public ToDoList? List { get; set; }
}
