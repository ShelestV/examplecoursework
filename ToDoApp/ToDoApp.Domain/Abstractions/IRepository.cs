﻿namespace ToDoApp.Domain.Abstractions;

public interface IRepository<TModel>
    where TModel : Model
{
    void Add(TModel model);
    void Delete(int id);
    void Update(TModel model);
    TModel? Get(int id);
    IReadOnlyCollection<TModel> GetAll();
}
