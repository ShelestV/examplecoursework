﻿namespace ToDoApp.Domain.Abstractions;

public abstract class Model
{
    public int Id { get; set; }
}
