﻿using ToDoApp.Domain.Abstractions;

namespace ToDoApp.Domain.Users;

public sealed class User : Model
{
    public string Name { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public string Role { get; set; } = string.Empty;

    public bool IsAdmin => Role.Equals(Roles.Admin);
}
