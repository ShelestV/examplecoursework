﻿namespace ToDoApp.Domain.Users;

public interface IUserRepository
{
    User? GetByName(string name);
    void Add(User user);
}
